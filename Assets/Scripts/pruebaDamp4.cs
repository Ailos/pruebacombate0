﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pruebaDamp4 : MonoBehaviour {

	/**
     * la cámara orbita con el stick derecho R3
     * al pulsar R3 la cámara se sitúa detrás del personaje
    */ 

	public Transform target;
	private float yVelocity, xVelocity;
	public float smooth = 0.3f;

	float yAngle, xAngle;
	bool giro = false;
	public float margen =5f;

	public float distancia = 6;
	public float otroAnguloY, otroAnguloX;
	//public float hSmooth = 150f;

	Vector3 destination = Vector3.zero;
	//--------------------------------------------------------
	[System.Serializable]
	public class InputSettings
	{ 
		public string ZOOM = "Mouse ScrollWheel";
		//public string ORBIT_HORIZONTAL_SNAP = "OrbitHorizontalSnap"; 
		public string ORBIT_HORIZONTAL  = "OrbitHorizontal";
		public string ORBIT_VERTICAL = "OrbitVertical";

	}

	//--------------------------------------------------------
	[System.Serializable]
	public class PositionSettings
	{
		public Vector3 targetPosOffset = new Vector3 (0, 2f, 0); //para que no mire a los pies. la cámara mira al origen-position del personaje (base en el suelo).  El offset lo levanta la altura del personaje
		public float lookSmooth = 100f; // igual que antes, como de rápido girar la cámara
		public float distanceFromTarget = -8; //cuanta distancia al objetivo. modificado por zooms.
		public float zoomSmooth =10; // como de rápido acercarnos a la cámara
		public float maxZoom = -2; // como de cerca y lejos del objetivo
		public float minZoom= -15;
	}
	//--------------------------------------------------------
	[System.Serializable]
	public class OrbitSettings
	{
		public float xRotation = -20;    //esta entrada modificará la rotación  
		public float yRotation = -180;
		public float maxXRotation = 45;
		public float minXRotation = -45; // solo acotamos la parte mirar arriba/abajo
		public float vOrbitSmooth = 150;
		public float hOrbitSmooth = 150; // how fast 
	}
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	public InputSettings input;
	public OrbitSettings orbit;
	public PositionSettings pos;
	float vOrbitInput, hOrbitInput;

	void Start () {
		yAngle = 0;
		xAngle = 0;

		input = new InputSettings ();
		orbit = new OrbitSettings ();
		pos = new PositionSettings ();

		vOrbitInput= 0;
		hOrbitInput= 0;

		otroAnguloY = 0;
		otroAnguloX = 0;

		yVelocity = xVelocity = 0;
	}

	void LateUpdate () {

		GetInput ();

		// 'giro' viene de GetInput
		if (giro) {

			yAngle = Mathf.SmoothDampAngle (transform.eulerAngles.y, target.eulerAngles.y, ref yVelocity, smooth); //interpola transform hasta que sea igual que target.
			xAngle = Mathf.SmoothDampAngle (transform.eulerAngles.x, target.eulerAngles.x, ref xVelocity, smooth); 

			transform.rotation = Quaternion.Euler (xAngle, yAngle, 0);
			transform.position = target.position + Quaternion.Euler (xAngle, yAngle, 0) * new Vector3 (0, 0, -distancia); //mejor reemplaza "Quaternion.Euler(xAngle, yAngle, 0)"  por  "transform.rotation"


			//resetear orbit		
			otroAnguloY = transform.eulerAngles.y ; //el orbit tiene que partir del sitio donde lo hemos dejado
			otroAnguloX = transform.eulerAngles.x;
			if (otroAnguloX > 180)
				otroAnguloX -= 360;

			/*
			sin esta resta de 360, al resetear un ángulo de -45 nos devolvía uno de 355, y al comparar con orbit.MaxRotation decía true y la cámara saltaba arriba.
			quizá se debería haber podido arreglar con Mathf.DeltaAngle(), pero no no salía bien. 
			*/

			//inmediatamente (frame siguiente) entra al else
		} else { 

			//orbitador. para cuando se maneja la cámara con el ratón (no hace falta damp)
			//otroAnguloY es acumulativo --> debería ser orbit.xRotation
			otroAnguloY += hOrbitInput * orbit.hOrbitSmooth * Time.deltaTime; //este era el truco --> otro Angulo es un número tan grande como se quiera. Q4.Euler lo pasa a angulo en grados.
			otroAnguloX += vOrbitInput * orbit.vOrbitSmooth * Time.deltaTime; 


			//que no se pase el anguloX
		
			if (otroAnguloX > orbit.maxXRotation){
				otroAnguloX = orbit.maxXRotation; 
			}

			if (otroAnguloX < orbit.minXRotation) {
				otroAnguloX = orbit.minXRotation;
			}

			destination = Quaternion.Euler (otroAnguloX, otroAnguloY, 0) * new Vector3 (0, 0, -distancia); 
			destination += target.position; 
			transform.position = destination;

			//orienta hacia el personaje
			transform.LookAt (target.position);
		}

	} // update

	void GetInput (){
		if ((Mathf.Abs(Mathf.DeltaAngle (transform.eulerAngles.y , target.eulerAngles.y)) < margen) && (Mathf.Abs(Mathf.DeltaAngle (transform.eulerAngles.x , target.eulerAngles.x)) < margen)) // alcanzado el sitio
			giro = false;

		// RE-CENTRAR
		if (Input.GetKey(KeyCode.Joystick1Button9)==true) 			
			giro = true;


		hOrbitInput = Input.GetAxisRaw (input.ORBIT_HORIZONTAL);
		vOrbitInput = Input.GetAxisRaw (input.ORBIT_VERTICAL); //raw: no necesitamos interpolar, solo estos valores -1,0,1
	
		}
} // class
/*
	al pulsar hacia abajo, además puede pusarse 'recentrar', la cámara sigue moviendose , esto es más una cuestión de requisitos y de que entradadas vamos a permitir
	simultáneamente con cuales.
*/