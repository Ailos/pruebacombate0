﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Salud : MonoBehaviour {

    public int puntosVida; // debe ser private
	public float tiempoDeCoolDown; //FireRate --> siempre debe ser mayor que el tiempo de desaparición de la partícula humo que se crea al golpear
    protected float ultima_herida;
    protected Combate player;

	//contiene el cerrojo
	public bool cooldown;

    // Use this for initialization
    void Start () {
        if (puntosVida < 3)
            puntosVida = 3;

        player = this.GetComponent<Combate>();

		cooldown = false;
		if (tiempoDeCoolDown==0 || tiempoDeCoolDown<0.35f)
			tiempoDeCoolDown =0.35f;

    }

    public int vidaRestante {
        get { return puntosVida; }
    }

   // es una cadencia tipo FireRate para q no pueda restarse toda la vida de un solo golpe
  /*  public bool puedeSerHerido()
    {
         return (ultima_herida + cadencia_herida) < player.ahora;
    } 
    */
  
   public void restarVida() {
        //herido o muerto
       // Debug.Log(Time.time + " " + this.name + " ¡¡¡HERIDO!!!");
        puntosVida--;

       // ultima_herida = player.ahora;
    }

	public IEnumerator coolDown(){
		cooldown = true;
		yield return new WaitForSeconds (tiempoDeCoolDown); //recomendado 0.9f
		cooldown = false;
	}


}// class
