﻿using UnityEngine;
using System.Collections;

[DisallowMultipleComponent] 			//imposible adjutarlo más de una vez al gameObject
[RequireComponent(typeof(Animator))] 	//se adjunta automáticamente el component requerido.
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]

public class MovementFreeform : MonoBehaviour {

	// 2D Freeform Directional
	/*moviento lateral, se desplaza de lado encarando al adversario. para combate (las animaciónes no son las más idóneas claro)
 	*/

	//private, por defecto
	Animator animator;	//referencia al Animator. 
	private float forwardInput, turnInput;
	public float umbralTurn;

	public float smoothTime = 0.3F;
	private float yVelocity = 0.0F;

	void Awake()
	{
		animator = GetComponent<Animator> ();
	}// Awake

	void Start(){
		forwardInput = turnInput = 0;
		if (umbralTurn ==0 )
			umbralTurn = 0.68f;
	}

	void Update()
	{
		GetInput ();
		Turning ();
		Move();
	}//Update


	//Recoger entradas
	void GetInput (){

		forwardInput = Input.GetAxis ("Vertical");
		turnInput = Input.GetAxis ("Horizontal");

		//float newPosition;
		//ayudita para el stick --> llegar a valor 1 o -1 (ya sabes q el Joystick en diagonal no puede darte el (1,1) no vale la pena pq la constelación en el arbol ya está definida sobre 0.6f
		//if (Mathf.Abs(forwardInput) > 0.7f) forwardInput= Mathf.Ceil(Mathf.Abs(forwardInput)) * Mathf.Sign(forwardInput); //no necesita lo de valor absoluto
		//if (Mathf.Abs(turnInput) > umbralTurn) turnInput= Mathf.Ceil(Mathf.Abs(turnInput) ) * Mathf.Sign(turnInput) ; //resultados pobres
		/*alternativa a ceiling
		 * if (Mathf.Abs (turnInput) > umbralTurn)
			newPosition = Mathf.SmoothDamp (turnInput,1*Mathf.Sign(turnInput), ref yVelocity, smoothTime);
		else
			newPosition = turnInput;
		animator.SetFloat ("MoveZ", newPosition);no vale la pena */
		//Debug.Log ("resultado tras ceiling f: " + forwardInput+ " t: "+turnInput + " n: " );
	}

	// escribir en los parameters del animator.
	void Move(){
		animator.SetFloat ("MoveX",forwardInput);
	}//Move

	void Turning (){
		animator.SetFloat ("MoveZ", turnInput);
	}//Turning
}//class
