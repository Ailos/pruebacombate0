﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inputs : MonoBehaviour {

    private Combate fighter;
	private Animator anim;
	// Use this for initialization
	void Start () {
		fighter = this.GetComponent<Combate>();
		anim = this.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		
		GetInput();

	} // update


	//---------------------------------------------------


	void GetInput(){		//GetInput siempre al update
		//solo vamos a leer entradas si no estamos en estado attack -- no es la manera más sencilla


	//	forwardInput = Input.GetAxis ("Vertical");
	//	turnInput = Input.GetAxis ("Horizontal");


		// si pulsas puño
		if ( Input.GetButtonDown("Button X") || Input.GetKeyDown(KeyCode.U) ){
		//	Debug.Log (this.name+"<color=green>PUÑETAZO</color>");
			//anim.SetTrigger ("Punch");
			fighter.lanzarAnimacionPunch();
		}
		// si pulsas patada
		if ( Input.GetButtonDown("Button B") || Input.GetKeyDown(KeyCode.I) ){
			//Debug.Log (this.name+"PATADA");
			//anim.SetTrigger ("Kick");
			fighter.lanzarAnimacionKick();
		}
		// si pulsas magia
		if ( Input.GetButtonDown("Button Y")|| Input.GetKeyDown(KeyCode.O) ){
			//anim.SetTrigger ("Magic");
			fighter.lanzarAnimacionMagic();
		}
		//si pulsas cubrir

		// si pulsas saltar L1
		if ((Input.GetKey(KeyCode.JoystickButton4)==true) || Input.GetKey(KeyCode.Q)){
			/*
			//anim.SetTrigger ("Jump1");	//mejor no usar trigger. tarda demasiado, da dos saltos. se usa para estados como "DIE" --> y tampoco conviene
			anim.SetBool ("Jump2", true);	//bool, necesita de isGrounded también o algo para poner la variable jump2 a false
			*/
			fighter.saltar();
		}

		//si pulsas agachar
		if ((Input.GetAxis("Axis 3") > 0f) || (Input.GetKeyDown(KeyCode.Z)) ){
			//Debug.Log ("agachar" );
			//anim.SetBool ("Crouch",true);	//puedes tratar un Bool como un trigger (admite settrigger)
			fighter.agachar(true);
		} 
		//si sueltas agachar - quizá puede ser un else
		if (/*(Input.GetAxis("Axis 3") < 0.9f)||*/(Input.GetKeyUp(KeyCode.Z))){   //o teclado o joystick. no se pueden comprobar estasd dos a la vez
			//anim.SetBool ("Crouch",false);
			fighter.agachar(false);
		}

		// CAMBIO ENTRE TRAVESIA Y COMBATE
		if (Input.GetKey (KeyCode.Joystick1Button8) || Input.GetKeyDown (KeyCode.P)  ) {
			fighter.cambioTravesiaCombate();
		}
	} // get input



} // class
