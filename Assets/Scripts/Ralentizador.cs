﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ralentizador : MonoBehaviour
{
    // Toggles the time scale between 1 and 0.7
    // whenever the user hits the Fire1 button.

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Debug.Log("ralentizando " +Time.time);
            if (Time.timeScale == 1.0f)
                Time.timeScale = 0.3f;
        }        
        if (Input.GetButtonDown("Fire2")) {
            Debug.Log("tiempo normal" + Time.time);
            Time.timeScale = 1.0f;
                // Adjust fixed delta time according to timescale
                // The fixed delta time will now be 0.02 frames per real-time second
                
            }
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
    }// update
}//class