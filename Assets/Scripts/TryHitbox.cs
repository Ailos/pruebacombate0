﻿using UnityEngine;
using System.Collections;

public class TryHitbox : MonoBehaviour {

	public Collider[] attackHitBoxes;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.G) )
			LaunchAttack (attackHitBoxes[0]);
	}//UPDATE

	private void LaunchAttack( Collider col){
		//col --> es el hitbox con el que pegas (la espada o el puño).
		//cols --> son los hibox que han recibido el golpe.
		Debug.Log("pulsar botón golpe" +LayerMask.GetMask ("HitBox"));
		Collider[] cols = Physics.OverlapBox (col.bounds.center, col.bounds.extents, col.transform.rotation, LayerMask.GetMask ("HitBox") );
		foreach(Collider c in cols){
				Debug.Log(c.name);	
			}
	}// LaunchAttack
}//class
