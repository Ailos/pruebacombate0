﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * este código se adjunta a los estados de la máquina de estados ( animator controller ) y de ahí va dando información o realizando acciones
*/

public class FighterBehaviour : StateMachineBehaviour {


	//variables para comunicar el estado 
	protected Combate fighter;// este referencia a la clase superior  <-- ¿no podría ser un public para ahorrar comprobaciones?
	public FighterState behaviourState; //este es el estado actual, entra por el desplegable de la interfaz animator controller;
    protected Salud salud;


	//public  Transform []guantes; --> no puedes meter esto en un StateMachineBehaviour


	//public override void OnStateMachineEnter()  para hacer el start

	// tú no tienes que hacer esta llamada , por tanto no hay q rellenar estos parámetros
	public override void OnStateEnter (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{ //equivalente a Start
		//Debug.Log ("estoy en estado "+behaviourState);
		//base.OnStateEnter (animator, stateInfo, layerIndex);

		if (fighter == null)
			fighter = animator.gameObject.GetComponent<Combate> ();
		
		//el estado en el que se encuentra el personaje es 'este'
		fighter.currentState = behaviourState;

		if(fighter.estaMuerto)
			fighter.target = null;

		//foreach (Transform g in guantes){
		//	g.gameObject.SetActive(false); //no puedo llegar hasta el collider pero no hace falta.
		//}

	}

	public override void OnStateUpdate (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{ //equivalente a Update...
      //base.OnStateUpdate (animator, stateInfo, layerIndex);
        fighter.currentState = behaviourState;
 //       if (fighter.name == "xbot") Debug.Log("<color=purple>estado On State Update " + stateInfo.tagHash + "</color>" );
            //Debug.Log("--> Durante OnStateUpdate <color=purple> el estado es "+ fighter.currentState + " / " +behaviourState +"</color>");
	}

   


    //parece q puede entrar varias veces al mismo estado sin salir del mismo
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex){
        
		//restar vida 
		if (salud == null)
			salud = animator.gameObject.GetComponent<Salud> ();
		if (fighter.beingHurted){
			salud.restarVida(); //restar puntos de vida en OnStateExit es la mejor manera de hacer esta operación atómica
		}
		// fighter.desactivarGuantes ();
	} // Exit 

} // class
