﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FighterState  {
	/* Separamos ATTACK EN 4
		-ATTACK_MA_IZQ	puño izquierdo
		-ATTACK_MA_DER	puño derecho
		-ATTACK_PIE_IZQ	pie izquierdo
		-ATTACK_PIE_DER	pie derecho
	*/
	ATTACK_MA_IZQ, ATTACK_MA_DER, ATTACK_PIE_IZQ, ATTACK_PIE_DER , IDLE, IDLE_TRAVELLER, DEAD, HURTED
}