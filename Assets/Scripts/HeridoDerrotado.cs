using UnityEngine;
using System.Collections;

public class HeridoDerrotado : MonoBehaviour {

	protected Animator anim;
	protected Combate player;
    public string nombreAnimacion; // en realidad es el nombre del parameter
    private Salud salud; // referencia al cerrojo

    //solo sirve para lanzar animaciones. Los cambios los produce el State Behaviour dentro del estado de la animación.
	protected void  Start (){
		anim = this.GetComponentInParent<Animator> ();
		player = this.GetComponentInParent<Combate> ();// --> hay que sacar las entradas para poder meterlo en un PNJ
	
        if (nombreAnimacion == null)
            nombreAnimacion = "golpeado";

        salud = this.GetComponentInParent<Salud>(); //contiene el cerrojo
	}

	
	public virtual void SerHeridoODerrotado (){
       // mensaje a Salud.cs

        if (!player.estaMuerto && /*salud.puedeSerHerido()&&*/ !player.beingHurted){   // quizá no hace falta  el beingHurted        

			anim.SetTrigger (nombreAnimacion);//este se queda aquí

            //puntosVida--; no es atómico
            //salud.restarVida(); haremos esto desde el Figheter state Behaviour. sino no es atómico

           // ultima_herida = player.ahora; este dato tiene q estar ahora en salud.cs

            //  Debug.Break();
            if (salud.vidaRestante<=0)
				anim.SetBool ("muerto", true);
				// + sustituir por ragdoll
		}// no muerto
	}// herido o derrotado 
    

    /*trasladamos la sección crítica al golpeador trigger
	void OnTriggerEnter (Collider col){		
		if (!salud.cooldown) {
			StartCoroutine (salud.coolDown() );
			//mi código
			this.GetComponent<Renderer>().material.SetColor("_Color",Color.yellow);
		}
	}*/

	void OnTriggerExit(){
		this.GetComponent<Renderer> ().material.SetColor ("_Color", Color.green);
	}

    
} // class