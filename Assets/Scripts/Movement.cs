﻿using UnityEngine;
using System.Collections;

[DisallowMultipleComponent] 			
[RequireComponent(typeof(Animator))] 	//se adjunta automáticamente el component requerido.
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]

public class Movement : MonoBehaviour {

	// 2D Simple Directional

	//private, por defecto
	Animator animator;	//referencia al Animator. 

	bool isWalking;

	void Awake()
	{
		animator = GetComponent<Animator> ();
			
	}// Awake , casi igual que Start

	void Update()
	{
		Turning ();
		Move();
	}//Update


	void Move(){
		animator.SetFloat ("MoveZ", Input.GetAxis("Vertical"));	//Se quita el snap del Axis vertical para evitar cambios bruscos en el movimiento por teclas
	}//Move

	void Turning (){
		animator.SetFloat ("MoveX", Input.GetAxis ("Horizontal")); //Se quita el snap del Axis horizontal para evitar cambios bruscos en el movimiento por teclas
	}//Turning
}//class
