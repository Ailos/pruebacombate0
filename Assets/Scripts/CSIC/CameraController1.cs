﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//https://www.youtube.com/watch?v=BBS2nIKzmbw
public class CameraController1 : MonoBehaviour {

	//este método lo que hace , es que la cámara siga al personaje, si se mueve o gira. No hace que podamos controlar la cámara con el raton <-- no hay inputs para la cámara

	public Transform target; // se trata de decirle a la cámara donde mirar.
	public Vector3 offsetFromTarget = new Vector3(0,6,-8); //como de lejos la camara del jugador.
	public float lookSmooth = 0.09f;
	public float xTilt = 10; //tilt-inclinación: la cámara está mas alta que el personaje, esto es cuanto queremos nuestra cámara rotar en el eje X( up - down), para que mire hacia abajo al personaje sin cámara orbit


	Vector3 destination = Vector3.zero; // hacia donde se mueve la cámara, la posición destino
	Combate charController; // referencia para coger la rotación
	float rotateXVel =0;
	float rotateYVel =0;

	// Use this for initialization
	void Start () {
		SetCameraTarget (target); // parece redundante, lo hacemos así por si queremos asignar con este código, nuevos objetivos.
	}


	//debería ser public para poder usarlo fuera de ESTE script
	void SetCameraTarget (Transform t ){ //dar  a la cámara la posibilidad de apuntar a OTRO target.
		target = t;
		if (target != null) {
			if (target.GetComponent<Combate> ()) {
				charController = target.GetComponent<Combate> ();

			} else
				Debug.LogError ("The camera's target needs a character controller.");
		} 
		else
			Debug.LogError ("your camera needs a target");
	}// set camera target



	void LateUpdate(){
		// solo despues de  todos los update --> LateUpdate is called after all Update functions have been called. This is useful to order script execution.
		// For example a follow camera should always be implemented in LateUpdate because it tracks objects that might have moved inside Update.
		//moving
		MoveToTarget();
		//rotating
		LookToTarget();

	}// late update




	// la variable charController es una mierda, podría ser solo target.rotation
	void MoveToTarget(){
		//mover la cámara detrás del objeto target ( en este caso el jugador)
		// estamos rotando un vector3 por la rotación dada ****
		destination = charController.TargetRotation * offsetFromTarget; // la rotación aumentada en su módulo hasta offset (detrás del personaje)
		//pero este punto rotado (rotación) puede estar en cualquiera parte del espacio 3d. (es decir, puede estar en 0,0,0  y el personaje en la posición 10, 30,-20)
		//entonces tenemos que hacer es añadimos este punto rotado a la posición de nuestro target
		destination += target.position; // nos aseguramos que la rotación es relativa a nuestro target desplazamiento)
		transform.position = destination; // pero esto no es mas que una variable. asígnarla a la posición de nuestra cámara
	}// move to target




	void LookToTarget(){
		float eulerY = Mathf.SmoothDampAngle (this.transform.eulerAngles.y, target.transform.eulerAngles.y, ref rotateYVel, lookSmooth);
		float eulerX = Mathf.SmoothDampAngle (this.transform.eulerAngles.x, target.transform.eulerAngles.x + xTilt, ref rotateXVel, lookSmooth);
		//Debug.Log ("eulerY " +eulerY+" es la diferencia entre "+this.transform.eulerAngles.y+":  "+target.transform.eulerAngles.y);
		transform.rotation = Quaternion.Euler (eulerX, eulerY, 0);
	

	/*	//con resta de vectores
		Vector3 targetPosition = target.position + new Vector3(0,2f,0); // no apuntar a los pies sino a la cabeza
		Quaternion targetRotation = Quaternion.LookRotation(targetPosition-transform.position);
		Debug.DrawRay (transform.position, targetPosition - transform.position, Color.green);
		transform.rotation = Quaternion.Lerp (transform.rotation, targetRotation,lookSmooth); // no entiendo porqué Lerp no usa Time.deltaTime
		*/
	} 



} // class
