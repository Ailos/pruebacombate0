﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine;
using System.Collections;

/*
 * ejemplo de uso del FromToRotation para cualquier entrada xyz
*/

public class fromTo : MonoBehaviour {
	public float x, y, z;

	void Start (){
		x = 10.0f;
		y = -5.0f;
		z = 3.0f;
	}
	void Update(){
	
		Example ();
	}
	void Example() {
		
		transform.rotation = Quaternion.FromToRotation(transform.forward, new Vector3(x, y,z))*transform.rotation; // y gira
	}
}