﻿using UnityEngine;
using System.Collections;

public class ExampleClass : MonoBehaviour {
	public Transform target;
	public float smooth = 0.3F;
	public float distance = 5.0F;
	private float yVelocity = 0.0F;

	public float a = 180; // podemos modificar desde el inspector
	void Update() {

		if (Input.GetKeyDown (KeyCode.R))
			if (a == 0)
				a = 180;
			else
				a = 0;
		if (a != 0) {
			float yAngle = Mathf.SmoothDampAngle (transform.eulerAngles.y, target.eulerAngles.y + a, ref yVelocity, smooth);
			Debug.Log ("EXAMPLE DAMP  transform " + transform.eulerAngles.y+" target "+target.eulerAngles.y +" damp es " +yAngle);
			Vector3 position = target.position;
			position += Quaternion.Euler (0, yAngle, 0) * new Vector3 (0, 0, -distance); //el suavizado está en la posición
			transform.position = position;
			transform.LookAt (target);
			//transform.rotation = target.rotation; //no es exactamente equivalente al look At. 
			/*si dejas que los ángulos transform.eulerAngles.y y target.eulerAngles.y se diferencien tanto, no se puede
		 * recuperar.
		*/
		}
	}
}
