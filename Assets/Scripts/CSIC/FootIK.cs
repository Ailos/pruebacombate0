﻿using UnityEngine;
using System.Collections;

public class FootIK : MonoBehaviour
{
	public LayerMask rayMask;
	public float legDistance = 0.4f;
	public float extraDistance = 0.4f;
	public float heightThreshold = 0.1f;
	public float footOffset = 0.16f;
	private Animator anim;
	private CharacterController controller;
	private Vector3 defCenter;
	private float defHeight;
	float lIKh;
	float rIKh;
	bool lHit;
	bool rHit;
	bool useLIK;
	bool useRIK;
	Vector3 lTan;
	Vector3 rTan;
	Vector3 lNrm;
	Vector3 rNrm;

	void Start()
	{
		anim = GetComponent<Animator>();
		controller = GetComponent<CharacterController>();
		defCenter = controller.center;
		defHeight = controller.height;
		controller.stepOffset = legDistance;
	}

	void OnAnimatorIK(int layer)
	{

		RayCastLeg(AvatarIKGoal.LeftFoot);
		RayCastLeg(AvatarIKGoal.RightFoot);

		Transform leftHeel = anim.GetBoneTransform(HumanBodyBones.LeftFoot);
		Transform rightHeel = anim.GetBoneTransform(HumanBodyBones.RightFoot);

		if(!lHit) lIKh = leftHeel.position.y;
		if(!rHit) rIKh = rightHeel.position.y;

		if(lHit || rHit)
		{
			float lowest = lIKh>rIKh?rIKh:lIKh;
			float highest = lIKh<rIKh?rIKh:lIKh;
			float diff = highest - lowest;
			controller.center = defCenter + new Vector3(0,diff / 2,0);
			controller.height = defHeight - diff;
		}
		else
		{
			controller.center = defCenter;
			controller.height = defHeight;
		}

		useLIK = lHit && (lIKh + heightThreshold > leftHeel.position.y);
		useRIK = rHit && (rIKh + heightThreshold > rightHeel.position.y);

		SetFootRotation(lHit,lTan,lNrm,AvatarIKGoal.LeftFoot);
		SetFootRotation(rHit,rTan,rNrm,AvatarIKGoal.RightFoot);

		SetFootPosition(useLIK,AvatarIKGoal.LeftFoot,leftHeel.position,lIKh);
		SetFootPosition(useRIK,AvatarIKGoal.RightFoot,rightHeel.position,rIKh);
	}

	private void RayCastLeg(AvatarIKGoal ag)
	{
		if(ag == AvatarIKGoal.LeftHand || ag == AvatarIKGoal.RightHand) return;
		bool h = false;
		RaycastHit hit;
		Transform heel = anim.GetBoneTransform(ag == AvatarIKGoal.LeftFoot?HumanBodyBones.LeftFoot:HumanBodyBones.RightFoot);
		Vector3 heelPos = heel.position;
		heelPos.y = (transform.position.y + heel.localPosition.y) + legDistance;
		if(Physics.Raycast(heelPos,Vector3.down,out hit,legDistance + extraDistance,rayMask.value))
		{
			h = true;
			if(ag == AvatarIKGoal.LeftFoot)
			{
				lHit = true;
				lIKh = hit.point.y;
				lTan = Vector3.Cross(hit.normal,-transform.right);
				lNrm = hit.normal;
			}
			else
			{
				rHit = true;
				rIKh = hit.point.y;
				rTan = Vector3.Cross(hit.normal,-transform.right);
				rNrm = hit.normal;
			}
		}
		if(!h)
		{
			if(ag == AvatarIKGoal.LeftFoot) lHit = false;
			else rHit = false;
		}
	}

	private void SetFootPosition(bool use,AvatarIKGoal ag,Vector3 heelPos,float IKh)
	{
		if(ag == AvatarIKGoal.LeftHand || ag == AvatarIKGoal.RightHand) return;

		if(use)
		{
			Vector3 Pos = new Vector3(heelPos.x,IKh + footOffset,heelPos.z);
			anim.SetIKPositionWeight(ag,1);
			anim.SetIKPosition(ag,Pos);
		}
		else
		{
			anim.SetIKPositionWeight(ag,0);
		}
	}

	private void SetFootRotation(bool h,Vector3 tan,Vector3 nrm,AvatarIKGoal ag)
	{
		if(ag == AvatarIKGoal.LeftHand || ag == AvatarIKGoal.RightHand) return;

		if(h)
		{
			Quaternion TanRot = Quaternion.LookRotation(tan,nrm);
			anim.SetIKRotationWeight(ag,1);
			anim.SetIKRotation(ag,TanRot);
		}
		else
		{
			anim.SetIKRotationWeight(ag,0);
		}
	}

	public bool isGrounded()
	{
		Vector3 tempNRM;
		return isGrounded(out tempNRM);
	}

	public bool isGrounded(out Vector3 nrm)
	{
		nrm = Vector3.up;
		Vector3 startPos = transform.position + controller.center;
		int mask = 1<<gameObject.layer;
		mask = ~mask;
		RaycastHit hit;
		if(Physics.SphereCast(startPos,controller.radius,Vector3.down,out hit,(controller.height/2 - controller.radius) + controller.contactOffset,mask))
		{
			nrm = hit.normal;
			float angle = 90-Vector3.Angle(Vector3.up,nrm);
			if(angle >= controller.slopeLimit)return true;
		}

		return false;
	}
}