﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//https://www.youtube.com/watch?v=txug_X8hlI0
public class CameraController2 : MonoBehaviour {

	//este método lo que hace hace que podamos controlar la cámara con el ratón. Además también sigue al personaje si se aleja

	public Transform target; // se trata de decirle a la cámara donde mirar.

	[System.Serializable]
	public class PositionSettings
	{
		public Vector3 targetPosOffset = new Vector3 (0, 2f, 0); //para que no mire a los pies. la cámara mira al origen-position del personaje (base en el suelo).  El offset lo levanta la altura del personaje
		public float lookSmooth = 100f; // igual que antes, como de rápido girar la cámara
		public float distanceFromTarget = -8; //cuanta distancia al objetivo. modificado por zooms.
		public float zoomSmooth =10; // como de rápido acercarnos a la cámara
		public float maxZoom = -2; // como de cerca y lejos del objetivo
		public float minZoom= -15;
	}

	[System.Serializable]
	public class OrbitSettings
	{
		public float xRotation = -20;    //esta entrada modificará la rotación  
		public float yRotation = -180;
		public float maxXRotation = 25;
		public float minXRotation = -85; // solo acotamos la parte mirar arriba/abajo
		public float vOrbitSmooth = 150;
		public float hOrbitSmooth = 150; // how fast 
	}

	[System.Serializable]
	public class InputSettings
	{ 
		// Input Axis --> añadirlos en Edit / Project Settings /Input
		public string ZOOM = "Mouse ScrollWheel";
		public string ORBIT_HORIZONTAL_SNAP = "OrbitHorizontalSnap"; //snap the y rotation of our camera back behind the target--> botón para volver a la posición detrás del personaje
		public string ORBIT_HORIZONTAL  = "OrbitHorizontal";
		public string ORBIT_VERTICAL = "OrbitVertical";

	}

	//referencias a estos settings
	public PositionSettings position = new PositionSettings();
	public OrbitSettings orbit = new OrbitSettings ();
	public InputSettings input = new InputSettings ();

	Vector3 targetPos = Vector3.zero; // targetPosOffset + target.position --> es un target.position artificial para apuntar a la cabeza del personaje y no a la base ( pies)
	Vector3 destination = Vector3.zero; // hacia donde se mueve la cámara, la posición destino

	Combate charController;
	float vOrbitInput, hOrbitInput, zoomInput, hOrbitSnapInput;

	//para la mejora en snap
	float rotateYVel =0;
	float lookSmooth = 0.09f;

	void Start () {
		SetCameraTarget (target); // parece redundante, lo hacemos así por si queremos asignar con este código, nuevos objetivos.

		//asegurarnos que empieza en la posición correcta --> es como una llamada a MoveToTarget()
		targetPos = target.position + position.targetPosOffset;
		destination = Quaternion.Euler (orbit.xRotation, orbit.yRotation, 0) * -Vector3.forward * position.distanceFromTarget;
		// entradas * -1,0,0(detras del target) * a cuánta distancia
		destination += targetPos; //desde la posición del target
		transform.position = destination; //las entradas preguntan que rotación
	}

	//debería ser public para poder usarlo fuera de ESTE script
	void SetCameraTarget (Transform t ){ //dar  a la cámara la posibilidad de apuntar a OTRO target.
		target = t;
		if (target != null) {
			if (target.GetComponent<Combate> ()) {
				charController = target.GetComponent<Combate> ();

			} else
				Debug.LogError ("The camera's target needs a character controller.");
		} 
		else
			Debug.LogError ("your camera needs a target");
	}// set camera target


	void Update(){
		//estas funciones no tienen cálculos de físicas ni dependen del personaje pj.
		GetInput();//respecto a atender entradas
		OrbitTarget ();
		ZoomInOnTarget ();
	}


	void GetInput(){
		vOrbitInput = Input.GetAxisRaw (input.ORBIT_VERTICAL);//no necesitamos interpolar, solo estos valores -1,0,1
		hOrbitInput = Input.GetAxisRaw (input.ORBIT_HORIZONTAL);
		hOrbitSnapInput = Input.GetAxisRaw (input.ORBIT_HORIZONTAL_SNAP);
		zoomInput = Input.GetAxisRaw(input.ZOOM); //"Mouse ScrollWheel" 
	}

	void LateUpdate(){
		// solo despues de  todos los update --> LateUpdate is called after all Update functions have been called. This is useful to order script execution.
		// For example a follow camera should always be implemented in LateUpdate because it tracks objects that might have moved inside Update.

		//moving
		MoveToTarget();
		//rotating
		LookToTarget();
		transform.LookAt(target);

	}// late update


	void MoveToTarget(){
		//seguir al personaje
		targetPos = target.position + position.targetPosOffset; //mira a la cabeza. no a la base del pj. por eso van a Update()
		//destination = Quaternion.Euler (orbit.xRotation, orbit.yRotation+ target.eulerAngles.y, 0) * -Vector3.forward * position.distanceFromTarget; // atiende al ratón y ademas copia el giro del personaje
		destination = Quaternion.Euler (orbit.xRotation, orbit.yRotation, 0) * -Vector3.forward * position.distanceFromTarget;  //atiende solo las entradas para el ratón
		destination += targetPos; //desde la posición del target
		transform.position = destination; //las entradas preguntan que rotación tenía la cámara.
	}// move to target


	void LookToTarget(){        
		//rotations no depende de target nunca más. solo de sí mismo
		Quaternion targetRotation = Quaternion.LookRotation(targetPos - transform.position);  // resta: vector que va desde transform.position a targetPos
		//aplicamos ahora esa rotación
		transform.rotation = Quaternion.Lerp(transform.rotation,targetRotation, position.lookSmooth*Time.deltaTime);

	}


	private float yVelocity = 0;

	void OrbitTarget(){
		if (hOrbitSnapInput > 0) {
			orbit.yRotation =  -180; //directamente detrás del personaje
		} 


		orbit.xRotation += -vOrbitInput * orbit.vOrbitSmooth * Time.deltaTime; // ES ACUMULATIVO
		orbit.yRotation += -hOrbitInput * orbit.hOrbitSmooth * Time.deltaTime; // el - creo q es pq si mueves el ratón a izquierda quieres mirar a derecha

		//controlar que la cámara no se pase ( no acabe bajo el personaje )
		if (orbit.xRotation > orbit.maxXRotation) {
			orbit.xRotation = orbit.maxXRotation;
		}
		if (orbit.xRotation < orbit.minXRotation) {
			orbit.xRotation = orbit.minXRotation;
		}

	}// orbit target

	void ZoomInOnTarget(){
		// como de cerca o lejos del objetivo.
		position.distanceFromTarget += zoomInput * position.zoomSmooth *Time.deltaTime;

		// no pasarse
		if( position.distanceFromTarget > position.maxZoom){
			position.distanceFromTarget = position.maxZoom;
		}
		if (position.distanceFromTarget < position.minZoom) {
			position.distanceFromTarget = position.minZoom;
		}
	}


} // class
