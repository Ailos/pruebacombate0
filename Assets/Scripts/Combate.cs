﻿using UnityEngine;
using System.Collections;
using System;

/**
 * 
 *  2d freeform cartesian
 * 
	el personaje se agacha y salta, y anda lateralmente - strafe ( encarado ).
	las series de puñetazos y patadas dependen del animador.
	*/

// https://www.youtube.com/watch?v=0apR5HwIfr8&index=2&list=PLZ7yoKGP5vkTqxvZgDwQ6XRKtGcw6A4bQ habla del fighter.cs
// https://www.youtube.com/watch?v=gC651-C8dSg&index=6&list=PLZ7yoKGP5vkTqxvZgDwQ6XRKtGcw6A4bQ FighterStateBehaviour.cs

public class Combate : MonoBehaviour {

	//con modificaciones del video camara orbit
	Quaternion targetRotation; 	// variable para la cámara que observa a este personaje.
	//Rigidbody rBody;

	//float forwardInput, turnInput;

	public float inputDelay = 0.1f;
	public float forwardvel = 12;
	public float rotateVel =100;
	public float cadencia_travesia = 0.5f; //FireRate
	public float cadencia_golpe = 0.8f;
	private float ultimo_cambio ; // entre combate y travesía
	private float ultimo_golpe ; // cada vez q doy un golpe

	// apuntar al enemigo
	public Transform target;
	private Vector3 alturaPersonaje;

	//empujar al enemigo
	public float fuerza;

	private Animator anim;

	// variables para controlar el estado
	public FighterState currentState;


	//referencia a los guantes
	public Transform [] guantes; //manos
	public Transform [] zapatillas; //pies
    


	//Preparar el terreno - inicializaciones
	void Awake(){
		anim = this.GetComponent<Animator>();
		//state = false;
		currentState = FighterState.IDLE;
		if (fuerza < 0)
			fuerza = 10f;



	}// AWAKE

	// Usar lo que hayas inicializado <- dar datos iniciales
	void Start () {
		targetRotation = transform.rotation; // la rotación de este objeto
		//rBody = this.GetComponent<Rigidbody>(); // en vez de un 'if'(no hay rigidBody) ponemos un require
	//	forwardInput = turnInput = 0; // inicialización múltiple

		//controlar el cambio entre travesia - combate
		ultimo_cambio = ahora;

		//ultimo golpe 
		ultimo_golpe = ahora;
//		desactivarGuantes (); esto da problemas

		alturaPersonaje = new Vector3 (0f, 1.5f, 0);

//travesia= false; 
	}// START
	
	//entradas como salto
	void Update () {
      
		//state = false;
	
/*		//volver del salto, ejecutar antes de GetInput()
		AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
		int estadoHash = stateInfo.fullPathHash;
		int jumpHash = Animator.StringToHash ("Superficie.jump"); //NO PODEMOS CAMBIAR TAN 'ALEGREMENTE' EL NOMBRE DE LAS LAYERS
		//Debug.Log ("en qué estado estamos ¿jump? " + (estadoHash == jumpHash)  );
		if (estadoHash == jumpHash)
			anim.SetBool ("Jump2", false); //igualmente sería bueno que hubiera un 'isGrounded'
		
		// esto habrá q revisarlo
		jumpHash = Animator.StringToHash ("Superficie.jump 0"); //jump en travesía
		if (estadoHash == jumpHash)
			anim.SetBool ("Jump2", false);
*/
		volverDelSalto();
        //desactivarGuantes ();

        //	GetInput ();
        //move
        //	Run ();

        //turn
        //	Turn ();

       // caminarEncarado1();
        caminarEncarado1();
	}//Update

    void caminarEncarado1() {

        //caminar encarado al enemigo (esto es una corrección para este modelo en concreto, ya que tiene el 'frente' de la figura) en el centro del personaje, y no a donde debería mirar para ver a su enemigo.¿seguro?
        if (target)
        {
            //transform.LookAt (target); --> esto hace que los personajes se tuerzan --> ¿y si pruebas con target2d?
            //volver las referencias 2d
            Vector3 target2d = new Vector3(target.position.x, 0, target.position.z); // no consideramos la y. solo dos dimensiones.
            Vector3 transform2d = new Vector3(transform.position.x, 0, transform.position.z);
            //aplica el giro.
            Quaternion direccion = Quaternion.LookRotation(target2d - transform2d, Vector3.up); //devuelve un giro
          
         
            //cuando mirar el objetivo.
            Debug.DrawRay(this.transform.position, target2d - transform2d, Color.red);
            if ((target2d - transform2d).magnitude > 0.5f)//si no está demasiado cerca
                transform.rotation = Quaternion.Lerp(transform.rotation, direccion, 0.8f); // ¡¡ gira !!
            if ((target2d - transform2d).magnitude > 15)//si está demasiado lejos
                target = null; //olvida el objetivo		
                
         //transform.LookAt (target2d); //funciona pero hace giros raros con diferentes alturas
        } //if
    } // caminarEncarado1


    // todo lo relacionado con físicas y rigidbody -> andar adelante.
    //void FixedUpdate(){	}
    /*

        void GetInput(){		//GetInput siempre al update
            //solo vamos a leer entradas si no estamos en estado attack -- no es la manera más sencilla


        //	forwardInput = Input.GetAxis ("Vertical");
        //	turnInput = Input.GetAxis ("Horizontal");


            // si pulsas puño
            if ((Input.GetButtonDown("Button X") || Input.GetKeyDown(KeyCode.U)) && (ultimo_golpe+cadencia_golpe)<ahora){
                Debug.Log (this.name+"PUÑETAZO");
                anim.SetTrigger ("Punch");
            }
            // si pulsas patada
            if (Input.GetButtonDown("Button B") || Input.GetKeyDown(KeyCode.I) ){
                Debug.Log (this.name+"PATADA");
                anim.SetTrigger ("Kick");
            }
            // si pulsas magia
            if (Input.GetButtonDown("Button Y")|| Input.GetKeyDown(KeyCode.O) ){
                anim.SetTrigger ("Magic");
            }
            //si pulsas cubrir

            // si pulsas saltar L1
            if (Input.GetKey(KeyCode.JoystickButton4) == true){
                //anim.SetTrigger ("Jump1");	//mejor no usar trigger. tarda demasiado, da dos saltos. se usa para estados como "DIE"
                anim.SetBool ("Jump2", true);	//bool, necesita de isGrounded también o algo para poner la variable jump2 a false
            }

            //si pulsas agachar
            if (Input.GetAxis("Axis 3") > 0f){
            //	Debug.Log ("agachar");
                anim.SetBool ("Crouch",true);	//puedes tratar un Bool como un trigger (admite settrigger)
            }

            if (Input.GetAxis("Axis 3") <0.9f){
                anim.SetBool ("Crouch",false);
            }

            // CAMBIO ENTRE TRAVESIA Y COMBATE
            if ((Input.GetKey (KeyCode.Joystick1Button8)   && ultimo_cambio+cadencia_travesia < ahora ) || (Input.GetKeyDown (KeyCode.P)   && ultimo_cambio+cadencia_travesia < ahora )) {
                ultimo_cambio = ahora;
                //cambiar entre modo combate / modo travesía
                //Debug.Log("cambiar modo combate travesia "  + travesia_combate+ " al tiempo " + ultimo_cambio);
                if (travesia_combate) {
                    //pasamos a combate
                    anim.SetBool("a_combate",true);
                } else {
                    //pasamos a travesía
                    anim.SetBool("a_combate",false);
                }

                //si había un objetivo de combate, olvídalo
                if (target)
                    target = null; 		// ESTE objeto quizá hay que sacarlo de estos scripts.  
            }
        } // get input
    */

    public Quaternion TargetRotation{
		get{ 
			return targetRotation;
		}	
	} 

	/*
	void Run(){
		if (Mathf.Abs (forwardInput) > inputDelay )
			rBody.velocity = transform.forward * forwardInput * forwardvel;
		else
			rBody.velocity = Vector3.zero;
	} // run


	void Turn (){
		//giro sobre sí mismo.
		if(Mathf.Abs (turnInput)>inputDelay){
			targetRotation *= Quaternion.AngleAxis (rotateVel * turnInput * Time.deltaTime, Vector3.up); //los grados de rotación tienen que multiplicarse porque sino, no se añade al giro anterior.
			transform.rotation  = targetRotation;
		}
	} // turn
	*/


	//----------- CONSULTAS DE ESTADOS -----------
	/*
	public bool attacking {
		//la idea es que devuelve true solo si está atacando
		get {
			return currentState == FighterState.ATTACK; 
		}
	} // attacking
	*/

	public bool attackingManoIzq {
		//la idea es que devuelve true solo si está atacando con el puño izquierdo
		get {
			return currentState == FighterState.ATTACK_MA_IZQ; 
		}
	} // attackingManoIzq

	public bool attackingManoDer {
		//la idea es que devuelve true solo si está atacando con el puño derecho
		get {
			return currentState == FighterState.ATTACK_MA_DER; 
		}
	} // attackingManoDer

	public bool attackingPieIzq {
		//la idea es que devuelve true solo si está atacando con el pie izquierdo
		get {
			return currentState == FighterState.ATTACK_PIE_IZQ; 
		}
	} // attackingPieIzq

	public bool attackingPieDer {
		//la idea es que devuelve true solo si está atacando con el pie derecho	
		get {
			return currentState == FighterState.ATTACK_PIE_DER; 
		}
	} // attackingPieDer



	public bool travesia_combate{ // ¡ es como una variable !
		get { 
			return currentState == FighterState.IDLE_TRAVELLER; //devuelve true si estamos en travesía
		}
	} // travesia_combate

	public float ahora  {
		get{ 
			return Time.time;
		}
	} // ahora


	public bool estaMuerto {
		get{ 
			return currentState == FighterState.DEAD;
		}
	}//muerto


	public bool beingHurted {
		//la idea es que devuelve true solo si está siendo golpeado ahora mismo.
		get {
			return currentState == FighterState.HURTED;  
		}
	} // herido

    public FighterState saberEstado() {
        return currentState;
    }



	//--------- FUNCIONES DE ATAQUE -------------------

	//esta función está mal
	public void lanzarAnimacionPunch (){
		
		if(( ultimo_golpe+cadencia_golpe)<ahora ){
			//activar puños
		/*	foreach (Transform g in guantes) { // podemos ahorrarnos el foreach
				//g.gameObject.SetActive (true);
				g.gameObject.GetComponent<Collider>().enabled = true;
			}
			*/
			//Debug.Log (this.name+" PUÑETAZO");
			anim.SetTrigger ("Punch");
		}		
	}//lanzar punch

	public void lanzarAnimacionKick (){
		
		if( (ultimo_golpe+cadencia_golpe)<ahora ){
			//activar pies
		/*	foreach (Transform g in zapatillas) {
				
				g.gameObject.GetComponent<Collider>().enabled = true;
			}*/
			//Debug.Log (this.name+" PATADA");
			anim.SetTrigger ("Kick");
		}		
	}//lanzar kick

	public void lanzarAnimacionMagic(){
		if( (ultimo_golpe+cadencia_golpe)<ahora ){
			anim.SetTrigger ("Magic");
		}	
	}

	//esta función está mal
	public void desactivarGuantes(){
	//	Debug.Log ("-----guantes desactivados ------");
		//desactiva los colliders de manos y pies.
		/*guantes[0].gameObject.SetActive(false);
		guantes[1].gameObject.SetActive(false);
		zapatillas[0].gameObject.SetActive(false); // así no se puede
		zapatillas[1].gameObject.SetActive(false);*/
		foreach (Transform g in guantes) {
			//g.gameObject.SetActive (false);
			g.gameObject.GetComponent<Collider>().enabled = false;
		}
		foreach (Transform g in zapatillas) {
			//g.gameObject.SetActive (false);
			g.gameObject.GetComponent<Collider>().enabled = false;
		}
	}

	//--------- FUNCIONES DE SALTO -----------

	public void saltar(){
		anim.SetBool ("Jump2", true);	//bool, necesita de isGrounded también o algo para poner la variable jump2 a false
	}


	private void volverDelSalto(){
		//volver del salto, ejecutar antes de GetInput()
		AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
		int estadoHash = stateInfo.fullPathHash;
		int jumpHash = Animator.StringToHash ("Superficie.jump"); //NO PODEMOS CAMBIAR TAN 'ALEGREMENTE' EL NOMBRE DE LAS LAYERS
		//Debug.Log ("en qué estado estamos ¿jump? " + (estadoHash == jumpHash)  );
		if (estadoHash == jumpHash)
			anim.SetBool ("Jump2", false); //igualmente sería bueno que hubiera un 'isGrounded'
		
		// esto habrá q revisarlo
		jumpHash = Animator.StringToHash ("Superficie.jump 0"); //jump en travesía
		if (estadoHash == jumpHash)
			anim.SetBool ("Jump2", false);

	}

	public void agachar(bool como){
		anim.SetBool ("Crouch",como);
	}

	//cambio entre travesia y combate

	public void cambioTravesiaCombate(){
		if (( ultimo_cambio+cadencia_travesia < ahora )) {
			ultimo_cambio = ahora;
			//cambiar entre modo combate / modo travesía
			//Debug.Log("cambiar modo combate travesia "  + travesia_combate+ " al tiempo " + ultimo_cambio);
			if (travesia_combate) {
				//pasamos a combate
				anim.SetBool("a_combate",true);
			} else {
				//pasamos a travesía
				anim.SetBool("a_combate",false);
			}

			//si había un objetivo de combate, olvídalo
			if (target)
				target = null; 		// ESTE objeto quizá hay que sacarlo de estos scripts.  
		}
	}

	//----
}//class