﻿using UnityEngine;
using System.Collections;


public class GolpeadorTriggerScript : MonoBehaviour {

	public GameObject humo;
	/*

	hay errores en este script. una vez q el player está attacking, cuanquier 'guante' puede marcar un punto de herida. aunq no sea el guante que está pegando.es decir 
	si damos un puñetazo pero un pie toca al adversario tambien marcará punto, así q restará 2 de vida.*/

	private Combate player; //se refiere al que golpea
	//private Animator anim ;
	private Collider guante;

	private float fuerza; //está muy lejos en la jerarquía para hacerlo public
    private float radio; //el radio del collider, para no inventarnoslo.

	private string nombre;//el nombre de este guante para hacer comparaciones
    private Animator animator;

	public float tiempoHumo;


	void Start (){
		player = this.gameObject.GetComponentInParent<Combate> ();

		fuerza = player.fuerza; // mínimo 500 para que se note

		guante = this.gameObject.GetComponent<Collider> (); // este 'guante' de boxeo
                                                            //si desonecto con setActive los guantes no puedo acceder a ellos luego
       // radio = guante.transform.localScale.y; // radio de este guante collider: es igual en todos los ejes --> pero no es lo que necesitamos
		nombre =guante.name;
        radio = 0.15f; // desde el punto de collision guante golpeador/ collider enemigo, ¿cuanto radio vamos a usar para comprobar? muy poco.
        animator = this.GetComponentInParent<Animator>();

		tiempoHumo = 0.45f;
	}

	void OnTriggerEnter(Collider objetoGolpeado)
	{

		//	si es el ataque4 y yo soy el guante 4....

		//golpeador de puño izquierdo
		if (player.attackingManoIzq && nombre=="GT_1" ) {
            funcionesDeGolpear(objetoGolpeado);
        }//if puño izquierdo 
		       
		//golpeador de puño derecho
		if (player.attackingManoDer && nombre=="GT_2") {
			funcionesDeGolpear(objetoGolpeado);
		}//if puño derecho 

		//golpeador de pie izquierdo
		if (player.attackingPieIzq && nombre=="GT_3") {
			funcionesDeGolpear(objetoGolpeado);
		}//if pie izquierdo 

        //golpeador de pie derecho
         if (player.attackingPieDer && nombre=="GT_4") {
			funcionesDeGolpear(objetoGolpeado);
		}//if pie derecho 
        
	}//on trigger
    
	void OnTriggerExit(){
		this.GetComponent<Renderer>().material.SetColor("_Color",Color.red);
	}


    void funcionesDeGolpear(Collider objetoGolpeado) {

        //diferenciar si lo que estas es tocando el suelo o algo que puedes golpear
        Collider[] cols = Physics.OverlapSphere(objetoGolpeado.bounds.center, radio, LayerMask.GetMask("Destruible")); //no hace falta --> hacer con el physics manager
        //objetoGolpeado solamente reacciona ante cosas de la capa destruible --> ya está filtrado que no soy 'yo', al menos por ahora.

		//colorear pq queda guay el guante golpeador
		if (cols.Length > 0)           
            this.GetComponent<Renderer>().material.SetColor("_Color", Color.yellow);
       
        // diferenciar entre colliders de varios enemimos o amigo --> NO. si toca varios enemigos ( o amigo) queremos zurrarles

		// diferenciar los colliders de la capa Destruible. Projet Settings / Physics / Physics Manager.
        // a este collider seleccionado, GOLPE.
   /*     Collider aux = null; //de momento solo sirve para un enemigo. Cuando queramos que golpee a dos enemigos, cambiar por un array de colliders y distinguir por el c.name
        float distanciaMinima = 10f; // distancia mínima golpeador/ golpeado.
        foreach (Collider c in cols)
        {//for de búsqueda
            Debug.DrawLine(this.transform.position, c.transform.position, Color.black); //línea distancia
             //elegir el menor de estas distancias, y sacar del for, este collider golpeado ganador --> un solo for por tanto.
            float distancia = (this.transform.position.magnitude - c.transform.position.magnitude);
            if (distancia < Mathf.Abs( distanciaMinima)) {
                distanciaMinima = distancia;
                aux = c;    //este es el collider golpeado mínimo al que enviar los mensajes
            }

       }//for
        //Debug.Log("el collider de distancia mínima es <color=red>" + aux.name + "</color> " + distanciaMinima);
*/
       // Combate enemigo = aux.GetComponentInParent<Combate>();
		//Salud salud = aux.GetComponentInParent<Salud>();
		Salud salud = objetoGolpeado.GetComponentInParent<Salud>();
       
        if (!salud.cooldown)
        {
            StartCoroutine(salud.coolDown()); //no permite que sea seleccionado otro trigger de coraza/casco/etc
            //mi código
            objetoGolpeado.gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.yellow);
            //aux.gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.clear);           
            GameObject polvo = (GameObject)Instantiate(humo, this.transform.position, this.transform.rotation);
            //aux.GetComponentInParent<HeridoDerrotado>().SerHeridoODerrotado();
			objetoGolpeado.GetComponentInParent<HeridoDerrotado>().SerHeridoODerrotado();
            Destroy(polvo, 0.45f);//este número no puede ser menor que el número de segundos que hay el en wait de coolDown()
        }
        
		   
            //probamos distintos empujones a ver cual mola más
            //c.GetComponent<Rigidbody> ().AddRelativeForce (fuerza*(player.transform.forward+player.transform.up)); 
            //c.GetComponent<Rigidbody> ().AddExplosionForce(fuerza, c.transform.position , 3.0f); 
            //FUERZA EXPLOSIVA es mejor dar la posición del contacto, desde un collision que desde un collider (OnCollissionEnter ()... ) 
            //Debug.DrawLine (player.transform.position, c.transform.position, Color.red);
       
    }//golpear    

} //class
