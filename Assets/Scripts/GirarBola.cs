﻿using UnityEngine;
using System.Collections;

public class GirarBola : MonoBehaviour {
	public float rotateVel = 0.3f;
	private float turnInput,forwardInput;
	public float inputDelay = 0.1f;


	public Transform[] cosas; // prueba a ver si podemos cogerlo

	void Start () {
		turnInput = forwardInput= 0;	
	}
	

	void Update () {
		GetInput ();
		Turn ();
	} // update

	void GetInput(){
		forwardInput = Input.GetAxis ("Vertical");
		turnInput = Input.GetAxis ("Horizontal");

	}

	void Turn (){
		//giro sobre sí mismo.
		if(Mathf.Abs (turnInput)>inputDelay || Mathf.Abs (forwardInput)>inputDelay){			
			transform.rotation *= Quaternion.AngleAxis (rotateVel * turnInput * Time.deltaTime, Vector3.up); //los grados de rotación tienen que multiplicarse porque sino, no se añade al giro anterior.

			transform.rotation *= Quaternion.AngleAxis (rotateVel * forwardInput * Time.deltaTime, Vector3.forward);  //en dos direcciones
		}
	} // turn
}// girar bola
