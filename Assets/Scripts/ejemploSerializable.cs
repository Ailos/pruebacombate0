﻿using UnityEngine;
using System.Collections;


public class ejemploSerializable : MonoBehaviour {

	[System.Serializable]
	public class Cosa{
		public float cualquierCosa = 45; //esta variable no podrá modificarse desde el inspector.
	}
	public Cosa cosita;
	// Use this for initialization
	void Start () {
		 cosita = new Cosa ();

	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log ("cosita es "+ cosita.cualquierCosa);
	}
}
