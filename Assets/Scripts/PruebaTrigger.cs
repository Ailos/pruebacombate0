﻿// Destroy everything that enters the trigger

using UnityEngine;
using System.Collections;

public class PruebaTrigger : MonoBehaviour
{
	//el objeto que lo contiene es trigger <-- si el objeto que lo contiene no es trigger pasan cosas raras (collider vs collider no reaccionaría) 
	// y el objeto que toca es collider o trigger
	//al menos UNO DE LOS DOS objetos, golpeador o golpeado DEBE TENER UN RIGIDBODY
	void OnTriggerEnter(Collider other)
	{
		Debug.Log ("DESTROY " + other.name);
		Destroy(other.gameObject);
	}
}