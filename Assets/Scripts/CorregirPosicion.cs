﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Este script sirve para proyectar la posición de cualquier figura sobre la altura del suelo. lo vamos a usar para objetos que no tienen su 'posicion' en la base, o para cuando los personajes vuelan.
 * se usa para que la orientación de persoanes miren a otros personajes a los que encaran en combate, sin torcerse, por ejemplo si uno está en el suelo y otro volando a 50m
*/
public class CorregirPosicion : MonoBehaviour {
	public Transform posicionBola ;
		
	// Update is called once per frame
	void Update () {
		this.gameObject.transform.position = new Vector3(posicionBola.position.x ,0, posicionBola.position.z);
		this.gameObject.transform.rotation = Quaternion.identity;
	}
}
